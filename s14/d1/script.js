// console.log("hello external");

// alert("Good afternoon!");

// let myName = "Mikha"; 
// ^ statement
// variable = myname
// console.log(myName);

/* NOTES
	- cannot declare the same variable under the same scope


*/



/* ANATOMY 
	
	** DECLARATION
	let <variable>
		- declaration
		- declaring a variable

		Example:
			let car;
			console.log("car");

			let car = "mercedez";
			console.log(car);


	** INITIALIZATION
		- initializing a variable with a value

		Example:
			let phone = "iPhone";
			console.log(phone);


	** RE-ASSIGNMENT
		- assigning new value to an exisisting variable using equal operator

		Example: 
			myName = "Gab";			----> reassigning variable
			console.log(myName);

		Example
			let brand = "Asus";
			brand = "Mac";

			console.log(brand);
			console.log(brand);
			console.log(brand);
			console.log(brand);
			console.log(brand);
				
*/

// console.log(lastName);
	// without 'let' keyword, return will be "NOT DEFINED"

// let firstName;
// console.log(firstName);
	// returns "UNDEFINED"

/* NAMING CONVENTION

	1. Case-sensitive

		let color = "pink";
		let Color = "blue";
		console.log(color);
		console.log(Color);

	2. Variable name must be relevant to its actual value
		
		let value ="Robin";
		let goodthief ="Robin"
		let bird ="Robin";

	3. Camel Case Notation
		
		let capitalcityofthephilippines = "Manila City";
		let capitalCityOfThePhilippines = "Manila City";

	4. 

		let year3 = 3;
		console.log(year3);
		
		let _year = 3;
		console.log (_year);

		// not accepted by JS
			let 3year = 3;
			let @year = year;

	-- single and double quotes are accepted

*/

/*

	CONSTANT
		- a type of variable that holds value but it cannot be changed nor reassigned	
	
	Example:
		const PI = 3.14;		---> must have declared value
		console.log(PI); 

*/



/*let country = `Philippines`;
let continent = `Asia`;
let population2020 = `109.6 million`;

console.log(country);
console.log(continent);
console.log(population2020);
*/


/* DATA TYPES

	1. String
		- sequence of characters
		- they are always wrapped with quotes or backticks
		- if no quotes or backsticks, JS will think of it as another variable

		let food = `sinigang`;
		`sinigang`	-> string

	2. Number
		- whole number (integer) and decimals (float)

		let x = 4;
		let y = 8;

		let result = x + y;
		console.log(result);

	3. Boolean
		- values TRUE or FALSE

		let isEarly = true;
		console.log(isEarly);

		let areLate = FALSE;
		console.log(areLate);

	4. Undefined
		- variable has been declared however no value yet
		- empty value

		let job;
		console.log(job)

	5. Null 
		- empty value
		- it is used to represent an empty value

	6. BigInt()
		- large integers
		- more than the data type can handle/hold

	7. Object 
		- one variable can handle/contain several different types of data
		- it is wrapped with curly braces
		- has property and value

		let user = {
			firstName: `Dave`,
			lastName: `Supan`,
			email: [
				`ds@mail.com`,
				`dave@gmail.com`,
				`dsupan@yahoo.com`
			],
			age: 16,
			isStudent: true,
			spouse: null
		}


	// special type of object
		// Array

		let fruit = [`apple`, `banana`, `strawberry`];
		let grades = [89, 90 92, 97, 95];

*/

/*
	typeof Operator
		- evaluates what type of data is that you are working with

	let animal = `dog`;
	let age = 16;
	let isHappy = true;
	let him;
	let spouse = null;
	let admin = {
		name: `Admin`,
		isAdmin: true
	};
	let ave = [83.5, 89.6, 94.2]

	console.log(typeof animal);
	console.log(typeof isHappy);
	console.log(typeof admin);
*/


/* FUNCTIONS
	
	- reusable
	- convenient 

	function sayHello(){
		console.log(`Hello World!`);
		console.log(`Hello World!`);
		console.log(`Hello World!`);
		console.log(`Hello World!`);
		console.log(`Hello World!`);
	}

	sayHello();
	console.log(`_______`);
	sayHello();

	* SYNTAX OF A FUNCTION

		function <fName>(){
	
			// statement / code block

		}


	* ANATOMY OF A FUNCTION

		1. Declaration
			- 'function' keyword
			- function name + parenthesis
				verb + description 
			- curly braces
				- determines its codeblock
				- statements are written inside the codeblock

		2. Invocation
			- invoke/ calls the function
			- by invoking the function, it executes the codeblock

			- function name + parenthesis
				-arguments inside the parenthesis

			function greeting(name){		---> "name" = parameter

				alert(`Hi ${name}!`);		---> simplifies the expression, performs arithmic, extract value
			}

			greeting(`Mikha`);		---> 'Mikha' = argument


*/

/*
function greeting(name){

	console.log(`Hi ${name}!`); 
}

greeting(`Mikha`);
greeting(`Ambrosio`);
*/


		/*function getProduct(x, y){

			console.log(`The product of ${x} and ${y} is ${x * y}`);

		}

		getProduct(2,5);
		getProduct(8,3);*/


		/*function getSum(x, y, z){

			console.log(`The sum of ${x} and ${y} and ${z} is ${x + y + z}`);

		}

		getSum(2,5,10);
		getSum(8,3,18);
		console.log(typeof getSum);*/


	/* FUNCTION WITH RETURN KEYWORD

		function sayName(fName, lName){

			return `Hi my name is ${fName} ${lName}`

		}

		// first method:
		console.log(sayName (`Mikhaella`, `Ambrosio`));

		// second method:
		let result = sayName (`Mikha`, `Ambrosio`);
		console.log(result);

	*/



	function sayIntro(name, age){

		return `Hi, I'm ${name}. My age (${age}) + 10 is ${10 + age}`

	}

	let hello = sayIntro(`Mikha` , 23);

	console.log(hello);



























































