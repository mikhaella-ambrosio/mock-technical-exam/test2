
/*	OPERATORS
		- Assignment
		- Arithmetic
		- Compound Assignment

		- Comparison
		- Logical
*/


		/*	ASSIGNMENT OPERATOR ( = )

				-used to assign a value to a variable

				let a = 14;
				console.log(a);

				a = 7;
				console.log(a);

				let b = a;
				console.log(b);

		*/

		/*	ARITHMETIC OPERATOR


				// Addition operator ( + )
						console.log(20 + 10);

				// Subtraction Operator ( - )
						console.log(20 - 10);

				// Multiplication Operator ( * )
						console.log(20 * 10);

				// Division Operator ( / )
						console.log(20 / 10);

				// Modulo Operator ( % )
						console.log(20 % 10);

				/* Increment ( ++ ) & Decrement ( -- )

				let c =30;

				console.log(++c);
				console.log(c);		//31

				console.log(--c);
				console.log(c);		//30

				console.log(c++);
				console.log(c);		//30

				console.log(c--);
				console.log(c);

				console.log(50 + 10);

				let a = 50;
				let b = 10;
				console.log(a + b);

				console.log(6 * (20-10));

		*/

		/*	COMPOUND ASSIGNMENT

				- perform arithmetic operation
				- assigning back the value to the variable 

				let f = 15

				// Addition Assignment Operator ( += )
						console.log(f += 3);	//18
						console.log(f += 3);	//21
						console.log(f += 3);	//24
						console.log(f);

				// Subtraction Assignment Operator ( -= )
						console.log(f -= 3);	//18
						console.log(f -= 3);	//21
						console.log(f -= 3);	//24
						console.log(f);

				// Multiplication Assignment Operator ( *= )
						console.log(f *= 3);	//30
						console.log(f *= 3);	//90
						console.log(f *= 3);	//360
						console.log(f);

				// Division Assignment Opearator ( /= )
						console.log(f /= 3);	//36
						console.log(f /= 3);	//9
						console.log(f /= 3);	//3
						console.log(f);
				
				// Modulo Assignment Opearator ( %= )
						f=200;
						console.log(f %= 11);	//2
						console.log(f %= );	//9
						console.log(f %= 3);	//3
						console.log(f);

		*/


		f=200;
						console.log(f %= 11);	//2
						console.log(f %= 1.5);	//9
						

















































