/*	
	SOLUTION 3 & 4

		- Create a trainer object using object literals
		- Initialize/add the following trainer object properties:
			- Name (string)
			- Age (number)
			- Pokemon(array)
			- Friends (objects with array values for properties)

*/

let trainer = {
	name: `Misty`,
	age: 10,
	pokemon: [`Goldeen`, `Staryu`, `Starmie`, `Togepi`, `Gyarados`],
	friends: {
		hometown: [`Daisy`, `Violet`, `Lily`],
		bestFriends: [`Ash`, `Brock`]
	},
	talk: function(){return `${this.pokemon[1]}! I choose you!`}
	
}
console.log(trainer)

console.log(`Result of dot notation:`);
console.log(trainer.name);

console.log(`Result of square bracket notation:`);
console.log(trainer[`pokemon`]);

console.log(`Result of talk method:`)
console.log(trainer.talk())


function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = level * 5;
	this.attack = level + 150;
}

let charmander = new Pokemon(`Charmander`, 22);	// goldeen
let cinderace = new Pokemon(`Cinderace`, 36);	// gyarados
let pidgey = new Pokemon(`Pidgey`, 11)	// togepi

console.log(charmander);
console.log(cinderace);
console.log(pidgey);


function tackle (targetPokemon, attackPokemon){

	console.log(`${targetPokemon.name} tackled ${attackPokemon.name}!`);

	let newHealthTarget = attackPokemon.attack - targetPokemon.health;

	console.log(`${targetPokemon.name}'s health is now reduced to ${newHealthTarget}`);

	if (newHealthTarget < 0){
		console.log(`${targetPokemon.name} has fainted.`)
	}
}

tackle(cinderace, charmander);
tackle(pidgey, charmander);





















