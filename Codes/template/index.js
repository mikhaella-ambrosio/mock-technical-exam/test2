function countLetter(letter, sentence) {
    // const validLetter = 'o';
    // const invalidLetter = 'abc';
    let count =0;
    
    
    if ( letter.length == 0) {
        let i =0;
        while (i< sentence.length){
            if (sentence.indexOf[i] == letter){
                count = count+1;
            } 
            i++;
        } return count
    } else {
        return undefined
    }
    // Check first whether the letter is a single character.

    //Conditons:
        // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
        // If letter is invalid, return undefined.

    
}


function isIsogram(text) {
    let i = text.length
    let j = 0;
    let k = j+1;
    for(j; j< i; j++){
        for(k; k <i ; k++){
            if(text[j] === text[k]){
                return false
            } else {
                return true
            }
            
        }
    }
   
    //Goal:
        // An isogram is a word where there are no repeating letters.

    //Check:
        // The function should disregard text casing before doing anything else.


    //Condition:
        // If the function finds a repeating letter, return false. Otherwise, return true.

    
}

function purchase(age, price) {
    const discountedPrice = price * 0.8;
    const roundedPrice = discountedPrice.toFixed(2);
    if (age < 13) {
        return undefined
    }
    else if ( (age >= 13 && age <= 21) || age >=65 ){
        return roundedPrice.toString()
    }
    else if ( age >= 22 && age <= 64){
        return price.toFixed(2).toString()
    }
    //Conditions:
        // Return undefined for people aged below 13.
        // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
        // Return the rounded off price for people aged 22 to 64.

    //Check:
        // The returned value should be a string.
    
}

function findHotCategories(items) {
    let i = 0;
    let result = []
    for(i; i< items.length; i++){
        if(items.stocks[i] == 0){
            result.push(items.category)
            break
        }
    }
    //Goal:
        // Find categories that has no more stocks.
        // The hot categories must be unique; no repeating categories.

    //Array for the test:
        // The passed items array from the test are the following:
        // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
        // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
        // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
        // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
        // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }


    //Expected return must be array:
        // The expected output after processing the items array is ['toiletries', 'gadgets'].

    // Note:
        // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

}

function findFlyingVoters(candidateA, candidateB) {
   
    let i =0;
    let j =0;
    let result = []
    for (i; i<candidateA.length ; i++){
        for (j; j<candidateB.length ; j++){
            if (candidateA[i] == candidateB[j]){
                result.push([candidateA[i]])
                return result
            }
        }
    }

    //Goal:
        // Find voters who voted for both candidate A and candidate B.

    //Array for the test:    
        // The passed values from the test are the following:
        // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
        // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // Expected return must be array:
        // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].

    //Note:
        // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};